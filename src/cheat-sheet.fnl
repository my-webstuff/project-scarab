#!/usr/bin/env fennel
(local utils (require :lib.scarab.utils))
;;(local tag (require :lib.scarab.tag))
(local tags (require :lib.scarab.tags))
(local tbl (require :lib.scarab.tbl))

(local fennel (require :fennel))
;;(print (fennel.searchModule :lib.scarab.utils fennel.path))
;;(print (fennel.searchModule :lib.scarab.tags fennel.path))
(local menu (require :modules.menu))


(local tcat utils.tcat)
(local pp utils.pp)
(local ++ utils.++)
(local -- utils.--)
(local kv utils.kv)
(local kvlist utils.kvlist)
;;(detangle test)

(local arguments
{:query (string.format "%q"  (or (os.getenv "QUERY_STRING") "no querry string"))
 :extra-path (string.format "%q" (or (os.getenv "PATH_INFO") "no extra path"))})

(local metaprops
	   [(tags.meta {:attribs {:charset  "utf-8"}})
		(tags.title "cheat-sheet")
		(tags.link-icons)
		(tags.link-css "styles/style.css")
		(tags.link-css "styles/base-grid.css")
		(tags.link-css "styles/main-grid.css")
		(tags.link-css "styles/menu-grid.css")
		(tags.link-feed "https://erik.lundstedt.it/updates.atom")
		])
;;(pp (tags.link-icons))
;;(pp (tags.link-css "styles/style.css"))

(local header (tags.header {:content (menu {:focused "cheat-sheet"})}))
;;(tags.nav {:content menu})}))

(local footer (tags.footer {:content []}))
(local sections
	   [{:attribs {:class "title"}
		 :content
		 [(tags.h1 {:text "title" })
		  (tags.p {:text "some description"})
		  (tags.p {:text arguments.query})]}
		{:attribs {:class "intro"}
		 :content
		 [(tags.h2 {:text "introduction"})
		  (tags.p {:text "this is some text"})
		  (tags.p {:text "here is some more text"})]}
		{:attribs {:class "parts part" :id "part-1"}
		 :content
		 [(tags.h2	{:text "lorem ipsum 1"})
		  (tags.p	{:text (tcat [] "<br>" )})]}
		{:attribs {:class "parts part" :id "part-2"}
		 :content
		 [(tags.h2 {:text "lorem ipsum 2"})
		  (tags.p  {:text (tcat [] "<br>" )})]}
		{:attribs {:class "parts part" :id "part-3"}
		 :content
		 [(tags.h2 {:text "lorem ipsum 3"})
		  (tags.hr {})
		  (tags.p  {:text (tcat [] "<br>")})]}])

(each [i v (ipairs sections)]
  (tset sections i (tags.section v)))

(local page
 [(tags.doctype)
  (tags.html
   [(tags.head {:content metaprops})
	(tags.body
	 {:content
	  [header
	   (tags.main
		{:attribs {:class (tcat ["grid-container" "main-grid"] " ")}
		 :content sections
		 })
	   footer]
	  :attribs {:class (tcat ["grid-container" "body-grid"] " ")}
	  })
	])])

(utils.headers)
;;(pp page)
(tags.detangle page)


