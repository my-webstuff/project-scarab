#!/usr/bin/env fennel
(local utils	(require :lib.scarab.utils))
(local tag		(require :lib.scarab.tag))
;;(local tags		(require :lib.scarab.tags))
;;(local tbl		(require :lib.scarab.tbl))

(local fennel (require :fennel))
;;(print (fennel.searchModule :lib.scarab.utils fennel.path))
;;(print (fennel.searchModule :lib.scarab.tags fennel.path))
(local menu (require :modules.menu))


(local tcat utils.tcat)
(local pp utils.pp)
(local ++ utils.++)
(local -- utils.--)
(local kv utils.kv)
(local kvlist utils.kvlist)

(lambda par [{: text :attribs ?attribs}]
  (tag.new tag {:tag "p" :content text :attribs ?attribs })
  )


(pp par)

(pp (par {:text "hi"}))

;;(tags.div {:children [(tags.p {:text "hello world"})]})
