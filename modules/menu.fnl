#!/usr/bin/env fennel

(local utils (require :lib.scarab.utils))
;;(local tag (require :lib.scarab.tag))
(local tags (require :lib.scarab.tags))
(local tbl (require :lib.scarab.tbl))

(local fennel (require :fennel))
;;(print (fennel.searchModule :lib.scarab.utils fennel.path))
;;(print (fennel.searchModule :lib.scarab.tags fennel.path))


(local tcat utils.tcat)
(local pp utils.pp)
(local ++ utils.++)
(local -- utils.--)
(local kv utils.kv)
(local kvlist utils.kvlist)
;;(detangle test)

(lambda menuItem [{: name : text : url : class : icon}]
  (tags.list.li {:content [(tags.a {:content [(tags.icon {: icon}) text] :href url})] :attribs {:class (tcat ["grid-item" class] " ") :id (.. "menu-item-" name)}} )
  (tags.div
   {:attribs {:class (tcat ["grid-item" "menu-item" class] " ")}
	:children
	[(tags.a {:content [(tags.icon {: icon :class ["icon"]}) text] :href url})]}))




(local navigation
	   [{:name "home"			:icon "home"		:text "Home"			:url "/index.fnl"		:class "home"}
		{:name "projects"		:icon "terminal"	:text "Projects"		:url "/projects.fnl"	:class "projects"}
		{:name "cheat-sheet"	:icon "code"		:text "Cheat-sheet"		:url "/cheat-sheet.fnl"	:class "cheat-sheet"}
		{:name "external links"	:icon "link"		:text "External links"	:url "/newtab.fnl"		:class "links"}
		{:name "blog"			:icon "rss_feed"	:text "Blog"			:url "/posts.fnl"		:class "blog"}])

(local menuItems [])

(fn menuItems-build [?focused]
  (icollect [_ item (ipairs navigation) &into menuItems]
    (do
      (when (or (= item.name ?focused) (= item.class ?focused))
        (set item.class (.. item.class " focused")))
      (menuItem item))))


(fn menu [{:focused ?focused}]
	   ;; (tags.list.ul
	   (tags.nav
		{:content
		 (menuItems-build ?focused)
		 :attribs {:class (tcat ["menu-container" "grid-container"] " ")}}))

;;(pp menu)



menu

