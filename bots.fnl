#!/usr/bin/env fennel
(local bots {})
(local utils {})
(local project (require :project))
(local projname project.name)
(local tcat table.concat)
;;(let [home (os.getenv "HOME") fennel (require :fennel)]  (set fennel.path (.. home "/botsdir/lib/" "?.fnl" ";" fennel.path)))
(let [fennel (require :fennel)] (print (fennel.searchModule :botcmd)))
(local cmd (require :botcmd))
;;(local cmd (require :lib.botcmd))
(require :botsetup)
(local rc (require :rc))
(local dirs rc.dirs)
(local settings {:webroot "/var/www/fennel/"})

(fn pp [o]
  (let [fennel (require :fennel)]
	(print (fennel.view o))))


(fn bots.init [self]
  (let [basedir (.. dirs.stow.src "/" project.name)
		mkdir cmd.mkdir]
	(mkdir {:dirs basedir})
	(each [_ v (pairs dirs.targets )]
	  (mkdir {:dirs (tcat [basedir "/" v])}))
	(mkdir {:dirs (tcat [basedir "/" dirs.targets.lib  project.name])})))

(set bots.setup bots.init)

(fn bots.build [self]
  (let [install cmd.install
		;;install pp
		name project.name
		bin dirs.targets.bin
		lib dirs.targets.lib
		stowdir dirs.stow.src
		projdefs dirs.targets.projdefs
		licensedir dirs.targets.licenses
		basedir (.. dirs.stow.src "/" project.name)]

	(each [k library (ipairs ["pair" "tag" "tags" "tbl" "utils" name])]
	  (install
	   {:src	(tcat ["lib/" name "/" library ".fnl"])
		:tgt	(tcat [basedir "/" lib name "/" library ".fnl"])}))

	;;(install {:src	(tcat [name ".fnl"])	:tgt (tcat [basedir "/" bin  name])})
	;;(cmd.exec ["cp" "-r" "lib/" "/var/www/fennel/lib"])
	 ;;{:src	(tcat [lib])	:tgt (tcat [basedir "/" bin  name])})
	(install {:src	"project.fnl"	:tgt (tcat [basedir "/" projdefs	name ".fnl"])})
	(install {:src	"LICENSE.org"	:tgt (tcat [basedir "/" licensedir  name ".org"])}))
  (print :done))

(λ bots.www [self]
  (let [install cmd.install
		sudo? false
		SUDO "";;"sudo "
		]
	
	;;(cmd.exec [ (SUDO)   "rm"  "-rf" "/var/www/fennel/"])
	(cmd.exec [SUDO "mkdir" "-pv" "/var/www/fennel/"])
	(cmd.exec [SUDO "mkdir" "-pv" "/var/www/fennel/modules"])
	(cmd.exec [SUDO "mkdir" "-pv" "/var/www/fennel/styles"])
	(cmd.exec [SUDO "mkdir" "-pv" "/var/www/fennel/lib/scarab"])
	(cmd.exec [SUDO "mkdir" "-pv" "/var/www/fennel/lib/scarab/enums"])
	(install {:sudo sudo? :src "src/*.fnl"				:tgt "/var/www/fennel/"})
	(install {:sudo sudo? :src "modules/*.fnl"			:tgt "/var/www/fennel/modules/"})
	(install {:sudo sudo? :src "styles/*.css"			:tgt "/var/www/fennel/styles/"})
	(install {:sudo sudo? :src "lib/scarab/*.fnl"		:tgt "/var/www/fennel/lib/scarab/"})
	(install {:sudo sudo? :src "lib/scarab/enums/*.fnl"	:tgt "/var/www/fennel/lib/scarab/enums/"})))
(set bots.try bots.www)



(lambda bots.html [self] (cmd.exec ["src/index.fnl|tee index.html"]))

(lambda bots.test [self] (cmd.exec ["tests/test-1.fnl"]))
(set bots.tests bots.test)

(lambda stow-settings [action] {:dir dirs.stow.src :target dirs.stow.tgt :pkgs [projname] : action})
(fn bots.stow	[self] (cmd.stow (stow-settings :stow)))
(fn bots.unstow	[self] (cmd.stow (stow-settings :unstow)))
(fn bots.clean [self]
  (let [name project.name
		;;exec cmd.tprint
		exec cmd.exec
		stowdir dirs.stow.src]
	(self:unstow)
	(exec ["rm " "-r " stowdir "/" name] "")))

(fn bots.install [self]
  (doto self
	;;(: :init)
	(: :build)
	(: :stow)))

(set bots.uninstall bots.unstow)
(fn bots.help [self]
  (print "#" "usage:")
  (print "*" "./bots.fnl <command>")
  (print "#" " commands:")
  (print "*" "init|setup" "preppare by creating missing directories in the filesystem, usually only needed on first run, or after a (ref:clean).")
  (print "*" " (un)stow " "Use GNU stow to stow(link) or unstow(unlink) the program, mostly used internally (ref:install) and for testing.")
  (print "*" "  install " (.. "build the program, copy the files to " dirs.stow.src " and finally, use GNU stow to symlink the files from " dirs.stow.src " to " dirs.stow.tgt "."))
  (print "*" "uninstall " "alias for unstow")
  )

(fn main [arg]
  (match arg
	(where (or :init :setup)) (bots:init)
	:build (bots:build)
	:clean (bots:clean)
	:stow							(utils.stow :stow)
	(where (or :uninstall :unstow)) (utils.stow :unstow)
	:install	(bots:install)
	:help		(bots:help)
	_			(bots:help)))


;;(pp arg)

;;(when (= (. arg 0) "./bots.fnl") (main (. arg 1)))

bots

