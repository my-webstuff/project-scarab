#!/usr/bin/env fennel

(local utils {})

(local pair (require :lib.scarab.pair))
(local tcat table.concat)
(set utils.tcat tcat)

(fn utils.pp [...] (let [fennel (require :fennel)] (print (fennel.view ...))))

(λ utils.++ [x] (+ x 1))
(λ utils.-- [x] (- x 1))

;;(lambda utils.kvlist [?attribs] (if ?attribs (tcat (icollect [_ kv (ipairs ?attribs)] (pair kv))) ""))
(lambda utils.kvlist [?attribs]
  (if ?attribs
	;;(utils.pp ["Z" ?attribs])
	;; (each [key value (ipairs ?attribs)]
	;;   (utils.pp ["a" key "b" value])
	;;   (each [k v (pairs value)]
	;; 	(string.format " %s=%q " k v)
	;; 	)
	;;   )
	  (do
		(local attribs ?attribs)
		(local properties [""])
		(icollect [key value (pairs attribs) &into properties]
		  (tcat [key "=\"" value "\""]))
		 (tcat properties " "))
	  ""
	 ))

(λ utils.headers []
  (print "Status: 200 OK")
  (print "Content-type: text/html")
  (print "\n")
)



utils
