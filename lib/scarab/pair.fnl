#!/usr/bin/env fennel

;;(local utils (require :lib.scarab.utils))




;;(lambda pair.new*
;;  [{:k		?k		:v		?v
;;	:key	?key	:val	?val
;;					:value	?value}]
;;  )


(lambda new [...]
  (local
   pair
   (case ...
	 {:k k :v v} {: k : v}
	 {:key key :val val} {:k key :v val}
	 {:key key :value value} {:k key :v value}
	 [k v] {:k k :v v}))
  ;;(fn pair.format [this] (string.format " %s=%q" this.k this.v))
  ;;(set pair.fmt pair.format)
  ;;(utils.pp pair)
  pair)

;;(new "a" "b")
;;(new {:k "a" :v "b"})
;;(new {:key "a" :val "b"})
;;(new {:key "a" :value "b"})

;;{: new }
new
