#!/usr/bin/env fennel
(local utils (require :lib.scarab.utils))
(local tag (require :lib.scarab.tag))


(local tcat utils.tcat)
(local pp utils.pp)
(local ++ utils.++)
(local -- utils.--)
(local kv utils.kv)
(local kvlist utils.kvlist)

(local tags {})



;;; core and meta tags
(lambda tags.doctype [] (tag:new {:notag true :tag "<!DOCTYPE html>" :content []}))
(lambda tags.html [content] (tag:new {:tag "html" : content}))
(lambda tags.head [content] (tag:new {:tag "head" : content}))
(lambda tags.title [title]  (tag:new {:tag "title" :content [title]}))
(lambda tags.meta [{: attribs}] (tag:new {:void true :tag "meta" :content [] : attribs }))
(lambda tags.body [{: content :attribs ?attribs}] (tag:new {:tag "body" : content :attribs ?attribs}))


;;; h<n>tags

(lambda tags.hgroup [{: lvl : content :attribs ?attribs}]
  (tag:new {:tag "hgroup" : content :attribs ?attribs}))

(λ tags.htag [{: lvl : text :attribs ?attribs}]
  (tag:new {:tag (.. "h" lvl) :content text :attribs ?attribs}))

(lambda tags.h [{: lvl : text :attribs ?attribs}]
  (tags.htag {: lvl :text (.. (string.rep "#" lvl) " " text) :attribs ?attribs}))

;;;; autogen h<n> tags and h<n>tag tags
(for [i 1 6]
  (tset tags (.. :h (tostring i))
   (lambda [{: text :attribs ?attribs}]
     {:fnl/docstring (.. "shorthand h" i " tag:new with nice styling.")}
     (tags.h {:lvl i : text :attribs ?attribs})))
  (tset tags (.. :h (tostring i) :tag)
   (lambda [{: text :attribs ?attribs}]
     {:fnl/docstring (.. "shorthand h" i "tag:new tag.")}
     (tags.htag {:lvl i : text :attribs ?attribs}))))

;;; common tags


;;;; semantic tags

(lambda tags.article [{: content :attribs ?attribs}]
  (tag:new {:tag "article" :content content :attribs ?attribs}))
(lambda tags.aside [{: content :attribs ?attribs}]
  (tag:new {:tag "aside" :content content :attribs ?attribs}))
(lambda tags.details [{: content :attribs ?attribs}]
  (tag:new {:tag "details" :content content :attribs ?attribs}))
(lambda tags.figcaption [{: content :attribs ?attribs}]
  (tag:new {:tag "figcaption" :content content :attribs ?attribs}))
(lambda tags.figure [{: content :attribs ?attribs}]
  (tag:new {:tag "figure" :content content :attribs ?attribs}))

(lambda tags.header [{: content :attribs ?attribs}]
  (tag:new {:tag "header" :content content :attribs ?attribs}))
(lambda tags.nav [{: content :attribs ?attribs}]
  (tag:new {:tag "nav" :content content :attribs ?attribs}))
(lambda tags.main [{: content :attribs ?attribs}]
  (tag:new {:tag "main" :content content :attribs ?attribs}))
(lambda tags.section [{: content :attribs ?attribs}]
  (tag:new {:tag "section" :content content :attribs ?attribs}))
(lambda tags.mark [{: content :attribs ?attribs}]
  (tag:new {:tag "mark" :content content :attribs ?attribs}))
(lambda tags.summary [{: content :attribs ?attribs}]
  (tag:new {:tag "summary" :content content :attribs ?attribs}))
(lambda tags.time [{: content :attribs ?attribs}]
  (tag:new {:tag "time" :content content :attribs ?attribs}))
(lambda tags.footer [{: content :attribs ?attribs}]
  (tag:new {:tag "footer" :content content :attribs ?attribs}))


;;;; style tags


(lambda tags.hr [{:content ?content :attribs ?attribs }]
  "horizontal rule
adds a horizontal line
"
  (tag:new {:void true :tag "hr" :content [] :attribs ?attribs})
  )

(lambda tags.br [{:attribs ?attribs }]
  "BReak line
Insert single line break in some text
"
  (tag:new {:void true :tag "br" :content [] :attribs ?attribs})
  )
;;(lambda tags.meta [{: attribs}] (tag:new {:void true :tag "meta" :content [] :attribs ?attribs }))


;;;; div
(lambda tags.div [{: children :attribs ?attribs}]
  (tag:new {:tag "div" :content children :attribs ?attribs}))

;;;; p,ptag
(lambda tags.p [{: text :attribs ?attribs}]
  (tag:new {:tag "p" :content text :attribs ?attribs }))
(set tags.ptag tags.p)
(set tags.paragraph tags.p)


;;;; list
(local list {})
(λ list.item [{: content :attribs ?attribs}]
	(tag:new {:tag "li" : content :attribs ?attribs }))
(set list.li list.item)
(lambda list.__new [{: tagname : content :attribs ?attribs }]
	(tag:new {:tag tagname : content :attribs ?attribs}))
(lambda list.ordered [{:content ?content :items ?items :attribs ?attribs}] (list.__new {:tagname "ol" :content (or ?items ?content) :attribs ?attribs}))
(set list.ol list.ordered)
(lambda list.unordered [{:content ?content :items ?items :attribs ?attribs}] (list.__new {:tagname "ul" :content (or ?items ?content) :attribs ?attribs}))
(set list.ul list.unordered)
(set tags.list list)



;;; extern
;;tags with external content, links images etc
;;;; link

(lambda tags.a [{: href : content :attribs ?attribs}]
  ;;(local properties (or ?attribs {}))
  ;;(tset properties :href href)
  (local properties {: href})
  (when ?attribs (collect [k v (pairs ?attribs) &into properties] k v))
  (tag:new {:tag "a" : content :attribs properties})
  )

(lambda tags.link [{: href : rel :type ?type :attribs ?attribs}]
  ;;(local properties (or ?attribs {}))
  ;;(tset properties :href href)
  (local properties {: href : rel :type ?type})
  (when ?attribs (collect [k v (pairs ?attribs) &into properties] k v))
  (tag:new {:void true :tag "link" :content [""] :attribs properties}))

(lambda tags.link-css [href ?attribs]
  (tags.link {: href :rel "stylesheet" :attribs ?attribs}))

(lambda tags.link-feed [href ?attribs]
  (tags.link {: href :rel "alternate" :type "application/rss+xml" :attribs ?attribs}))

;;;; img,image
(lambda tags.img* [url alt width height]
  "creates the equivalent html-tag. this is an image"
  (.. "\t\t" "<img src=\"" url "\" alt=\"" alt "\" width=\"" width
	  "\" height=\"" height "\">" "\n")
  (tag:new {:void true :tag "img" :content "" :attribs {:src url : alt : width : height}}))

(lambda tags.img [{: url : alt } {:width width :height height} {:id ?id :class ?class}]
  (tag:new {:void true :tag "img" :content "" :attribs {:src url : alt : width : height :id ?id :class ?class}}))

(lambda tags.image [image]
  (tags.img {:url image.url :alt image.alt} {:width image.width :height image.height} {:id image.id :class image.class}))

;;; icon-stuff
(λ tags.link-icons []
  (tags.link-css "https://fonts.googleapis.com/icon?family=Material+Icons"))

(λ tags.icon [{: icon :class ?class :id ?id}]
  (local class [])
  (table.insert class "material-icons")
  (when ?class (icollect [_ c (ipairs ?class) &into class] c))
  (tag:new {:tag "span" :content icon :attribs {:class (tcat class " ") :id ?id}})
  )

;;; other functions

(lambda tags.detangle [tbl ?i]
  (let [i (or ?i 0)]
	(each [k val (pairs tbl)]
	  ;;(pp {:abc val})
	  (match (type val)
		:table ;;(do (pp [:X val ]) (pp [:Y (getmetatable val)])
		(if (or val.tag val.name)
			(tags.detangle (tag.htmlize val) (++ i))
			(tags.detangle val (++ i)));;)
		:string
		(if (> i 1)
			(print (.. (string.rep "  " i) val))
			(print val))))))

tags
