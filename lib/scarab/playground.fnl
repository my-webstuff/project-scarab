#!/usr/bin/env fennel
(local utils (require :lib.scarab.utils))
(local tags (require :lib.scarab.tags))

(local tcat utils.tcat)
(local pp utils.pp)
(local kv utils.kv)
(local kvlist utils.kvlist)


(when false
  (fn door [key lock]
	(local wall {:door {: key : lock}})
	(setmetatable wall {:__call (lambda [] (print wall.door.key wall.door.lock))}))
  (local front-door (door "somekey" "impossible-lock"))
  (front-door) (pp front-door)
  (set front-door.door.lock "possible lock")
  (front-door) (pp front-door)
  )
(when false
  (local plist
		 (tags.div
		  [(tags.ptag "hey there 1")
		   (tags.ptag "hey there 2")
		   (tags.ptag "hey there 3")
		   (tags.ptag "hey there 4")]))
  )
(when false
  (local attribs {:a "Alice" :b "Bob" :foo "bar" :lorem "ipsum"})
  (local properties [])

  (icollect [key value (pairs attribs) &into properties]
	(tcat [key "=\"" value "\""]))


  (print (tcat properties " "))

  )


(pp (tags.icon {:icon "home"}))


;;(local divtag (tags.div [""]))

;;(pp plist)
;;(pp (plist))
;;(tags.detangle (plist))
