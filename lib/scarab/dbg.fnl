#!/usr/bin/env fennel
(local nova (require :supernova))
(local utils (require :lib.scarab.utils))
(local tcat utils.tcat)
(local pp utils.pp)
(local kv utils.kv)
(local kvlist utils.kvlist)
(local tbl (require :lib.scarab.tbl))
(local loglvl (require :lib.scarab.enums.loglvl))

(lambda block [text] (tcat ["[" text "]"] ""))

(lambda echo [this ...] (when this.__echo (print ...)))

(lambda info [this ...] (when (>= this.level loglvl.info) (print (block "info") ...)))

(lambda debug [this ...] (when (>= this.level loglvl.debug) (print (block (nova.blue.bg-yellow "debug")) ...)))

(lambda warning [this ...] (when (>= this.level loglvl.warning) (print (block (nova.bg-red "warn")) ...)))

(lambda err [this ...] (when (>= this.level loglvl.error) (print (block (nova.bold.bg-red "error")) ...)))





(lambda new [{: level}]
  {: level
   : info
   : echo 
   : debug :dbg debug
   : warning
   : err :error err
   :echo-off (lambda [this] (set this.__echo false))
   :echo-on (lambda [this] (set this.__echo true))
   :__echo true
   : echo
   }
  )
(when (= (. arg 1) "test")
  (local dbg (new {:level loglvl.normal}))
  (dbg:info "hey there!")
  (dbg:info "this is some info")
  (dbg:echo "hey")
  (dbg:echo "there!")
  ;;(dbg:echo-on)
  (dbg:echo-off)
  (dbg:echo "was that an error?")

  (dbg:debug "this is some debug info")
  (dbg:warning "this is a warning")
  (dbg:err "THIS IS AN ERROR MESSAGE"))

{: new}
