#!/usr/bin/env fennel

{:log 1
 :low 2
 :normal 3 :medium 3
 :inf 3 :info 3
 :dbg 3 :debug 3 
 :high 10
 :warn 10 :warning 10
 :crit 50 :critical 50
 :err 50 :error 50
 :all 100 :trace 100}
