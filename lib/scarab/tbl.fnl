#!/usr/bin/env fennel
(local utils (require :lib.scarab.utils))
;;(local pp utils.pp)

(local tbl [])
(local tcat table.concat)
(set tbl.cat tcat)

;;(lambda tbl.merge [a b] (collect [i j (pairs b) &into a] i j) a)

(lambda tbl.merge [a b] (each [key value (pairs b)] (tset a key value)) a)
(lambda tbl.imerge [a b] (each [key value (ipairs b)] (tset a key value)) a)



;;(λ tbl.imerge [a b] (icollect [i j (ipairs b) &into a] j) a)

;;(pp (tbl.merge {:a "foo" :b "bar"} {:lorem "ipsum" :dor "sit"}))
;;(pp (tbl.imerge [:a "foo" :b "bar"] [:lorem "ipsum" :dor "sit"]))

tbl
