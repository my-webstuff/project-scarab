#!/usr/bin/env fennel

(local utils (require :lib.scarab.utils))
(local tcat utils.tcat)
(local pp utils.pp)
(local kv utils.kv)
(local kvlist utils.kvlist)
(local tbl (require :lib.scarab.tbl))

(local debug (require :lib.scarab.dbg))
(local loglvl (require :lib.scarab.enums.loglvl))
(local dbg (debug.new {:level loglvl.normal}))

(local tag {})

;;(lambda tag* [{: tag : content :attribs ?attribs :notag ?notag}]	(if ?notag [tag] [(.. "<" tag (kvlist properties) ">") content (.. "</" tag ">")]		))


(lambda tag.htmlize [tag]
  ;;(pp ["x" attribs])
  ;;(pp ["y" (kvlist attribs)])
  (if
   tag.notag
   [tag.tag]
   ;;elseif
   tag.void
   [(.. "<" tag.tag (kvlist tag.attribs) ">")]
   ;;else
   [(.. "<" tag.tag
		(kvlist tag.attribs)
		">")
	tag.content
	(.. "</" tag.tag ">")]))

(lambda tag.new* [{: tag : content :attribs ?attribs :notag ?notag :void ?void}]
  ;; (local attribs {})
  (local attribs (or ?attribs []))
  ;; (when ?attribs ;;(print "attribs") (pp ?attribs)
  ;; 	(each [i v (ipairs ?attribs)] (table.insert attribs v )))
  ;;(pp ["a" attribs ?attribs])
  ;; (when ?extra-attribs ;;(print "extra-attribs") (pp ?extra-attribs)
  ;; 	(each [i v (ipairs ?extra-attribs)] (table.insert attribs v)))
  ;;(tbl.merge attribs ?attribs)
  
  ;;(print "***************************************************************")
  ;;(pp ["b" attribs ?extra-attribs])
  
  (local newtag {: tag : content :attribs ?attribs :notag ?notag :void ?void})
  ;;(fn stringer [])
  (setmetatable newtag {:__name (.. "name: " tag)
						:__istag true
						;;:__call call
						}))
  
(lambda tag.new [this {:tag name : content :attribs ?attribs :notag ?notag :void ?void}]
  (let [obj {: name :tag name : content :attribs (or ?attribs []) :notag ?notag :void ?void}]
	(set this.__name (.. "name: " name))
	(set this.__istag true)
	(setmetatable obj this)
	(set this.__index this)
	obj))

;; (lambda par [{: text :attribs ?attribs}]
;;   (tag.new tag {:tag "p" :content text :attribs ?attribs })
;;   )


;; (pp par)

;; (pp (par {:text "hi"}))

;;(setmetatable tag {:__call new})
tag
;;tag.new
