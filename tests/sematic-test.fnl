#!/usr/bin/env fennel
(local fennel (require :fennel))
(local utils (require :lib.scarab.utils))
;;(local tag (require :lib.scarab.tag))
(local tags (require :lib.scarab.tags))
(local tbl (require :lib.scarab.tbl))

(local tcat utils.tcat)
(local pp utils.pp)
(local ++ utils.++)
(local -- utils.--)
(local kv utils.kv)
(local kvlist utils.kvlist)
;;(detangle test)

(let [ptag (tags.p {:text	"test"}	)]
  (assert (= ptag.content	"test"	))
  (assert (= ptag.tag		"p"		)))

